/**
 * 
 */
package com.opensource.nredis.proxy.monitor.zookeeper.enums;

/**
 * @author liubing
 *
 */
public enum ZkNodeType {
	
	 	AVAILABLE_SERVER,
	    UNAVAILABLE_SERVER,
	    CLIENT
}
